package passwordValidatorTest;

import static org.junit.Assert.*;

import org.junit.Test;

import passwordValidator.PasswordValidate;

/*
 * Name:Dilraj Singh
 * Student Number: 991547818
 
 * This Test class will verify if the passwords entered will fulfill the following requirements
 1.	A password must have at least 8 characters
 2.	A password must contain at least two digits
 
 The class will be created using TDD.
 * */

public class PasswordValidateTest {

	@Test
	public  void testHasValidCaseCharsRegular() {
		//fail("Invalid case characters");
		boolean result = PasswordValidate.hasValidCaseChars("BBbb");
		assertTrue("Invalid case characters", result);
	}
	
	@Test
	public  void testHasValidCaseCharsBoundaryIn() {
		//fail("Invalid case characters");
		boolean result = PasswordValidate.hasValidCaseChars("zZ");
		assertTrue("Invalid case characters", result);
	}
	
	
	@Test
	public  void testHasValidCaseCharsExceptionBlank() {
		//fail("Invalid case characters");
		boolean result = PasswordValidate.hasValidCaseChars("");
		assertFalse("Invalid case characters", result);
	}
	
	@Test
	public  void testHasValidCaseCharsExceptionNull() {
		//fail("Invalid case characters");
		boolean result = PasswordValidate.hasValidCaseChars(null);
		assertFalse("Invalid case characters", result);
	}
	
	@Test
	public  void testHasValidCaseCharsExceptionNumbers() {
		//fail("Invalid case characters");
		boolean result = PasswordValidate.hasValidCaseChars("123456");
		assertFalse("Invalid case characters", result);
	}
	
	@Test
	public  void testHasValidCaseCharsBoundaryOutUpper() {
		//fail("Invalid case characters");
		boolean result = PasswordValidate.hasValidCaseChars("ZZZZZZZ");
		assertFalse("Invalid case characters", result);
	}
	
	@Test
	public  void testHasValidCaseCharsBoundaryOutLower() {
		//fail("Invalid case characters");
		boolean result = PasswordValidate.hasValidCaseChars("zzzzzz");
		assertFalse("Invalid case characters", result);
	}
	
	
	//*****************************   BREAK    *************************************
	
	@Test
	public void testIsValidLengthRegular() {
		// fail("invalid Length for password!");
		assertTrue("invalid length", PasswordValidate.isValidLength("1234567890"));
	}

	@Test
	public void testIsValidLengthException() {
		assertFalse("invalid length", PasswordValidate.isValidLength(null));
	}

	@Test
	public void testIsValidLengthExceptionSpaces() {
		assertFalse("invalid length", PasswordValidate.isValidLength("        "));
	}

	@Test
	public void testIsValidLengthExceptionBoundaryIn() {

		assertTrue("invalid length", PasswordValidate.isValidLength("Password"));
	}
	
	@Test
	public void testIsValidLengthExceptionBoundaryOut() {

		assertFalse("invalid length", PasswordValidate.isValidLength("Passwrd"));
	}
	
	
	@Test
	public void testHasEnoughDigitsRegular() {
		//fail("Less than two digits");
		assertTrue("Less than two digits", PasswordValidate.hasEnoughDigits("Pass8or4"));
	}
	
	@Test
	public void testHasEnoughDigitsException() {
		//fail("Less than two digits");
		assertFalse("Less than two digits", PasswordValidate.hasEnoughDigits("Pass-1or4"));
	}
	
	
	@Test
	public void testHasEnoughDigitsBoundaryIn() {
		//fail("Less than two digits");
		assertTrue("Less than two digits", PasswordValidate.hasEnoughDigits("Pass8or46"));
	}
	
	@Test
	public void testHasEnoughDigitsBoundaryOut() {
		//fail("Less than two digits");
		assertFalse("Less than two digits", PasswordValidate.hasEnoughDigits("Pass8ord"));
	}

}
